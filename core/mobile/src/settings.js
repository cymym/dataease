module.exports = {
    PUBLICKEY: 'public_key',
    TokenKey: 'Authorization',
    RefreshTokenKey: 'refreshauthorization',
    LinkTokenKey: 'LINK-PWD-TOKEN',
    title: '华森制药工业互联网平台',
    WHITE_LIST: [
      '/api/auth/login',
      '/api/auth/getPublicKey',
      '/system/ui/info',
      '/system/ui/image/',
      'pages/tabBar/me/index'

    ],
    RECENT_KEY: 'recently',
    USER_INFO_KEY: 'user-info'
  }
  