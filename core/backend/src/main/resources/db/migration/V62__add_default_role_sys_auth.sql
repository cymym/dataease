DELETE FROM sys_auth WHERE auth_user = 'admin-role';

INSERT INTO sys_auth (
SELECT UUID(), id, 'panel', 2, 'role', 20230818102209000, NULL, 'admin-role', NULL, NULL, NULL FROM
(SELECT resource_id AS id FROM panel_link_mapping WHERE UUID IN
  (SELECT SUBSTRING(`path`, 7) FROM sys_menu WHERE model = 1 AND `path` LIKE '/link/%' AND title IN ('党建域'))
) AS t
WHERE t.id NOT IN
(SELECT auth_source FROM sys_auth WHERE auth_source_type = 'panel' AND auth_target = 2 AND auth_target_type = 'role')
);

INSERT INTO sys_auth (
SELECT UUID(), id, 'panel', 2, 'role', 20230818102209000, NULL, 'admin-role', NULL, NULL, NULL FROM
(SELECT pid as id FROM panel_group WHERE id IN
  (SELECT auth_source FROM sys_auth WHERE auth_source_type = 'panel' AND auth_target = 2 AND auth_target_type = 'role')
  AND pid NOT IN
  (SELECT auth_source FROM sys_auth WHERE auth_source_type = 'panel' AND auth_target = 2 AND auth_target_type = 'role')
) AS t
);

INSERT INTO sys_auth (
SELECT UUID(), id, 'panel', 2, 'role', 20230818102209000, NULL, 'admin-role', NULL, NULL, NULL FROM
(SELECT pid as id FROM panel_group WHERE id IN
  (SELECT auth_source FROM sys_auth WHERE auth_source_type = 'panel' AND auth_target = 2 AND auth_target_type = 'role')
  AND pid NOT IN
  (SELECT auth_source FROM sys_auth WHERE auth_source_type = 'panel' AND auth_target = 2 AND auth_target_type = 'role')
) AS t
);

INSERT INTO sys_auth (
SELECT UUID(), id, 'panel', 2, 'role', 20230818102209000, NULL, 'admin-role', NULL, NULL, NULL FROM
(SELECT pid as id FROM panel_group WHERE id IN
  (SELECT auth_source FROM sys_auth WHERE auth_source_type = 'panel' AND auth_target = 2 AND auth_target_type = 'role')
  AND pid NOT IN
  (SELECT auth_source FROM sys_auth WHERE auth_source_type = 'panel' AND auth_target = 2 AND auth_target_type = 'role')
) AS t
);

INSERT INTO sys_auth (
SELECT UUID(), id, 'panel', 2, 'role', 20230818102209000, NULL, 'admin-role', NULL, NULL, NULL FROM
(SELECT pid as id FROM panel_group WHERE id IN
  (SELECT auth_source FROM sys_auth WHERE auth_source_type = 'panel' AND auth_target = 2 AND auth_target_type = 'role')
  AND pid NOT IN
  (SELECT auth_source FROM sys_auth WHERE auth_source_type = 'panel' AND auth_target = 2 AND auth_target_type = 'role')
) AS t
);
INSERT INTO sys_auth (
  SELECT UUID(), id, 'dataset', 2, 'role', 20230818102209000, NULL, 'admin-role', NULL, NULL, NULL FROM dataset_group
  WHERE id IN ('14d18d3f-0af7-46b4-aa60-315a6f2dc4c1')
);
INSERT INTO sys_auth (
  SELECT UUID(), id, 'dataset', 2, 'role', 20230818102209000, NULL, 'admin-role', NULL, NULL, NULL FROM dataset_table
  WHERE scene_id IN ('14d18d3f-0af7-46b4-aa60-315a6f2dc4c1')
);



INSERT INTO sys_auth (
  SELECT UUID(), id, 'panel', 20, 'role', 20230818102209000, NULL, 'admin-role', NULL, NULL, NULL FROM panel_group
);

INSERT INTO sys_auth (
  SELECT UUID(), id, 'dataset', 20, 'role', 20230818102209000, NULL, 'admin-role', NULL, NULL, NULL FROM dataset_group
);
INSERT INTO sys_auth (
  SELECT UUID(), id, 'dataset', 20, 'role', 20230818102209000, NULL, 'admin-role', NULL, NULL, NULL FROM dataset_table
);

INSERT INTO sys_auth (
  SELECT UUID(), id, 'link', 20, 'role', 20230818102209000, NULL, 'admin-role', NULL, NULL, NULL FROM datasource
);


DELETE FROM sys_auth_detail WHERE create_user = 'admin-role';
INSERT INTO sys_auth_detail (
  SELECT UUID(), id, 'i18n_auth_use', 1, 1, 'use', '基础权限-使用', 'admin-role', 1689933023000, NULL, NULL, NULL FROM sys_auth
  WHERE auth_user = 'admin-role'
);
