package io.dataease.service.sys;


import io.dataease.auth.api.dto.CurrentUserDto;
import io.dataease.commons.exception.DEException;
import io.dataease.commons.utils.AuthUtils;
import io.dataease.commons.utils.BeanUtils;
import io.dataease.controller.sys.request.RoleGridRequest;
import io.dataease.controller.sys.response.RoleUserItem;
import io.dataease.ext.ExtMenuMapper;
import io.dataease.ext.ExtSysRoleMapper;
import io.dataease.ext.ExtSysUserMapper;
import io.dataease.plugins.common.base.domain.SysRole;
import io.dataease.plugins.common.base.mapper.SysMenuMapper;
import io.dataease.plugins.common.request.KeywordRequest;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SysRoleService {

    @Resource
    private ExtSysRoleMapper extSysRoleMapper;
    @Resource
    private ExtSysUserMapper extSysUserMapper;

    public List<SysRole> query(KeywordRequest request) {
        return extSysRoleMapper.query(request);
    }

    public List<RoleUserItem> allRoles() {
        return extSysRoleMapper.queryAll();
    }


    public boolean checkRoleNameUnique(RoleGridRequest role) {
        SysRole sysRole = extSysRoleMapper.queryByName(role.getName());
        if (ObjectUtils.isNotEmpty(sysRole) && !sysRole.getRoleId().equals(role.getId())) {
            return false;
        }else {
            return true;
        }
    }

    /**
     * 保存角色信息
     * @param role
     * @return
     */
    public int insertRole(RoleGridRequest role) {
        CurrentUserDto user = AuthUtils.getUser();
        SysRole sysRole = new SysRole();
        BeanUtils.copyBean(sysRole,role);
        sysRole.setCreateBy(ObjectUtils.isNotEmpty(sysRole)?user.getUsername():"");
        int ret = extSysRoleMapper.insertRole(sysRole);
        List<Map<String, Long>> params = new ArrayList<>();
        role.getMenuList().forEach(value->{
            Map<String,Long> param = new HashMap<>();
            param.put("roleId", sysRole.getRoleId());
            param.put("menuId",value);
            params.add(param);
        });
        if (!params.isEmpty()) {
            extSysRoleMapper.batchInsertRoleMenu(params);
        }
        return ret;
    }

    /**
     * 修改角色
     * @param role 角色信息
     * @return 影响的数据行数
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateRole(RoleGridRequest role) {
        SysRole sysRole = extSysRoleMapper.queryById(role.getId());
        if (ObjectUtils.isEmpty(sysRole)){
            DEException.throwException("该角色["+role.getName()+"]不存在");
        }
        sysRole.setName(role.getName());
        sysRole.setDescription(role.getDescription());
        // 获取当前用户
        CurrentUserDto user = AuthUtils.getUser();
        sysRole.setUpdateBy(user.getUsername());
        // 修改角色信息
        int row = extSysRoleMapper.updateRole(sysRole);
        // 删除角色与菜单关联
        extSysRoleMapper.deleteRoleMenu(role.getId());
        List<Map<String, Long>> params = new ArrayList<>();
        role.getMenuList().forEach(value->{
            Map<String,Long> param = new HashMap<>();
            param.put("roleId", sysRole.getRoleId());
            param.put("menuId",value);
            params.add(param);
        });
        if (!params.isEmpty()) {
            extSysRoleMapper.batchInsertRoleMenu(params);
        }
        return row;
    }

    public void checkRoleAllowed(RoleGridRequest role) {
        if (ObjectUtils.isNotEmpty(role.getId()) && 1L == role.getId())
        {
            DEException.throwException("不允许操作超级管理员角色");
        }
    }

    /**
     * 批量删除角色
     * @param roleIds 需要删除的角色ids
     * @return 影响的数据行数
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteRoleByIds(Long[] roleIds) {
        for (Long roleId : roleIds)
        {
            checkRoleAllowed(new RoleGridRequest(roleId));
            SysRole role = extSysRoleMapper.queryById(roleId);
            if (countUserRoleByRoleId(roleId) > 0)
            {
                DEException.throwException(String.format("%1$s已分配,不能删除", role.getName()));
            }
        }
        // 删除角色与菜单关联
        extSysRoleMapper.deleteRoleMenuByRoleIds(roleIds);
        return extSysRoleMapper.deleteRoleByIds(roleIds);
    }

    /**
     * 通过角色ID查询角色使用数量
     * @param roleId
     * @return
     */
    private int countUserRoleByRoleId(Long roleId) {
       return extSysUserMapper.countUserRoleByRoleId(roleId);
    }
}
