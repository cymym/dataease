package io.dataease.ext;

import io.dataease.plugins.common.base.domain.SysMenu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ExtMenuMapper {

    @Update(" update sys_menu set sub_count = sub_count+1 where menu_id = #{menuId} ")
    int incrementalSubcount(@Param("menuId") Long menuId);

    @Update(" update sys_menu set sub_count = sub_count-1 where menu_id = #{menuId} and sub_count > 0")
    int decreasingSubcount(@Param("menuId") Long menuId);

    /**
     * 查询用户拥有的所有菜单
     * @param userId
     * @return
     */
    List<SysMenu> selectMenuListByUserId(Long userId);

    /**
     * 查询角色关联的菜单
     * @param roleId 角色id
     * @return
     */
    List<Long> selectMenuListByRoleId(Long roleId);

    /**
     * 根据角色id查询关联的菜单
     * @param roleIds 角色ids
     * @return 菜单ids
     */
    List<Long> selectMenuListByRoleIds(@Param("roleIds") List<Long> roleIds);

    /**
     * 删除菜单与角色的关联
     * @param menuId 菜单id
     * @return 影响的行数
     */
    @Delete("delete from sys_roles_menus where menu_id = #{menuId}")
    int deleteMenuRoleRelation(Long menuId);
}
