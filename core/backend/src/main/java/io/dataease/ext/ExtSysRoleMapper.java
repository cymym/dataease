package io.dataease.ext;

import io.dataease.controller.sys.response.RoleUserItem;
import io.dataease.plugins.common.base.domain.SysRole;
import io.dataease.plugins.common.request.KeywordRequest;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface ExtSysRoleMapper {


    List<SysRole> query(KeywordRequest request);

    int deleteRoleMenu(@Param("roleId") Long roleId);
    int deleteRoleMenuByRoleIds(Long[] roleId);

    int batchInsertRoleMenu(@Param("maps") List<Map<String, Long>> maps);

    List<RoleUserItem> queryAll();

    List<Long> menuIds(@Param("roleId") Long roleId);

    SysRole queryByName(@Param("name") String name);

    /**
     * 保存角色信息
     * @param role
     * @return
     */
    int insertRole(SysRole role);

    /**
     * 修改角色信息
     * @param role
     * @return 影响行数
     */
    int updateRole(SysRole role);

    /**
     * 根据角色id查询角色信息
     * @param id 角色id
     * @return {@link SysRole}
     */
    SysRole queryById(@Param("roleId") Long id);

    /**
     * 删除角色
     * @param roleIds
     * @return 影响的数据行数
     */
    int deleteRoleByIds(Long[] roleIds);

    /**
     * 查询角色相关的菜单ids
     * @param roleId 角色id
     * @return 菜单ids
     */
    List<Long> selectRelateMenuIdsByRoleId(Long roleId);
}
