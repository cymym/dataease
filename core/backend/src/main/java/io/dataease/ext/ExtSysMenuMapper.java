package io.dataease.ext;

import io.dataease.controller.sys.request.SimpleTreeNode;
import io.dataease.ext.query.GridExample;
import io.dataease.plugins.common.base.domain.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ExtSysMenuMapper {

    List<SimpleTreeNode> allNodes();


    List<SysMenu> querySysMenu();

    List<SimpleTreeNode> nodesByExample(GridExample gridExample);

    List<SysMenu> querySysMenu(@Param("array") Long[] models, @Param("userId") Long userId);
}
