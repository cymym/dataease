package io.dataease.controller.sys.response;

import io.dataease.plugins.common.base.domain.SysRole;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ym.c
 * @date 2023/7/17 10:45
 */
@Data
public class SysRoleGridResponse extends SysRole {
    @ApiModelProperty("ID")
    private Long id;
}
