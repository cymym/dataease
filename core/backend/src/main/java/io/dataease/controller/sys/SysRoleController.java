package io.dataease.controller.sys;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import io.dataease.auth.annotation.SqlInjectValidator;
import io.dataease.commons.exception.DEException;
import io.dataease.commons.utils.PageUtils;
import io.dataease.commons.utils.Pager;
import io.dataease.controller.sys.request.RoleGridRequest;
import io.dataease.plugins.common.base.domain.SysRole;
import io.dataease.plugins.common.request.KeywordRequest;
import io.dataease.service.sys.SysRoleService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色信息
 * 
 * @author ruoyi
 */
@RestController
@RequestMapping("/api/role")
public class SysRoleController
{
    @Autowired
    private SysRoleService sysRoleService;
    @ApiOperation("查询用户角色")
    @RequiresPermissions("role:read")
    @PostMapping("/roleGrid/{goPage}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "goPage", value = "页码", required = true, dataType = "Integer"),
            @ApiImplicitParam(paramType = "path", name = "pageSize", value = "页容量", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "request", value = "查询条件", required = true)
    })
    @SqlInjectValidator(value = {"create_time", "name"})
    public Pager<List<SysRole>> list(@PathVariable int goPage, @PathVariable int pageSize,
                                                 @RequestBody KeywordRequest request)
    {
        Page<Object> page = PageHelper.startPage(goPage, pageSize, true);
        List<SysRole> users = sysRoleService.query(request);
        return PageUtils.setPageInfo(page, users);
    }

    @ApiOperation("新增角色")
    @RequiresPermissions("role:add")
    @PostMapping
    public int add(@Validated @RequestBody RoleGridRequest role)
    {
        if (!sysRoleService.checkRoleNameUnique(role))
        {
            DEException.throwException("新增角色'" + role.getName() + "'失败，角色名称已存在");
        }
        return sysRoleService.insertRole(role);
    }

    /**
     * 修改保存角色
     */
    @ApiOperation("修改角色")
    @RequiresPermissions("role:edit")
    @PutMapping
    public int edit(@Validated @RequestBody RoleGridRequest role)
    {
        sysRoleService.checkRoleAllowed(role);
        if (!sysRoleService.checkRoleNameUnique(role))
        {
            DEException.throwException("修改角色'" + role.getName() + "'失败，角色名称已存在");
        }
        return sysRoleService.updateRole(role) ;
    }

    /**
     * 删除角色
     */
    @ApiOperation("删除角色")
    @RequiresPermissions("role:del")
    @DeleteMapping("/{roleIds}")
    public int remove(@PathVariable Long[] roleIds)
    {
        return sysRoleService.deleteRoleByIds(roleIds);
    }
}
