package io.dataease.controller.sys.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
public class RoleGridRequest implements Serializable {
    private Long id;
    @NotBlank(message = "角色名称不能为空")
    @Size(min = 0, max = 30, message = "角色名称长度不能超过30个字符")
    private String name;

    private String description;

    private List<Long> menuList;

    public RoleGridRequest(Long id) {
        this.id = id;
    }
}
