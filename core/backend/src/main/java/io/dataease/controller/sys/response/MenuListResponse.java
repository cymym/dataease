package io.dataease.controller.sys.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ym.c
 * @date 2023/7/18 1:45
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MenuListResponse implements Serializable {

    private static final long serialVersionUID = -6171754350276356612L;
    /**
     * id
     */
    private Long id;
    /**
     * 父级id
     */
    private Long parentId;
    /**
     * 菜单类型
     */
    private Integer type;

    /**
     * 菜单名
     */
    private String title;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否是外链
     */
    private Boolean frame;

}
