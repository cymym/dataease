package io.dataease.auth.entity;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MDMUser implements Serializable {

    /**
     * batchId : 1171648548608448
     * items : [{"pkId":"1135406383779168","RY_WORKINFO":[{"orgName":"华森集团MDM集成测试","endDate":"","abnormalEvents":"入职","changeType":"","isInPosition":"Y","positionCode":"G00837","isTrial":"N","depName":"测试02","depCode":"1009001","positionName":"软件测试工程师","isMainPosition":"N","employeeType":"01","orgCode":"230706","startingDate":"2023-04-24"}],"RY_ZT":{"natrueOfRegister":"0","documentType":"1","nation":"01","workEmail":"huangwei@pharscin.com","documentNumber":"500382198704103418","birthdates":"1987-04-10","depNameZG":"供应商三维测试","highestEducation":"21","employeeID":"00808","orgNameZG":"华森集团MDM集成测试","salaryCardNumber":"6230520470062156875","recruitmentChannels":"04","isOnTheJob":"Y","salaryCardSubBranch":"中国农业银行双碑支行","salaryCardBank":"中国农业银行","highestVQ":"","positionCodeZG":"G00832","emergencyContactNum":"18716425723","depCodeZG":"23070601","registerResidenceNum":"90000004","registerResidence":"渝北","highestDegree":"","namePinyin":"huangwei","emergencyContact":"唐迎迎","sex":"M","orgCodeZG":"230706","highestPT":"","CODE":"huangwei","registerAddress":"90000004","politicalLandscape":"04","phoneNumber":"18225322775","nationality":"CHN","currentResidence":"重庆市江北区石马河街道国惠路32号8-9-2","name":"黄伟","nativePlace":"重庆合川","IDAddress":"重庆市江北区石马河街道国惠路32号8-9-2","employeeTypeZG":"正式员工","isSpecialGroup":"01","maritalStatus":"已婚","seniorityDate":"2023-04-24","positionNameZG":"测试2"}}]
     * splitPage : {"total":"1","pageNumber":"1","totalPages":"1","pageSize":"10"}
     */

    private String batchId;
    private SplitPageBean splitPage;
    private List<ItemsBean> items;

    @Data
    public static class SplitPageBean implements Serializable {
        /**
         * total : 1
         * pageNumber : 1
         * totalPages : 1
         * pageSize : 10
         */

        private String total;
        private String pageNumber;
        private String totalPages;
        private String pageSize;
    }

    @Data
    public static class ItemsBean implements Serializable {
        /**
         * pkId : 1135406383779168
         * RY_WORKINFO : [{"orgName":"华森集团MDM集成测试","endDate":"","abnormalEvents":"入职","changeType":"","isInPosition":"Y","positionCode":"G00837","isTrial":"N","depName":"测试02","depCode":"1009001","positionName":"软件测试工程师","isMainPosition":"N","employeeType":"01","orgCode":"230706","startingDate":"2023-04-24"}]
         * RY_ZT : {"natrueOfRegister":"0","documentType":"1","nation":"01","workEmail":"huangwei@pharscin.com","documentNumber":"500382198704103418","birthdates":"1987-04-10","depNameZG":"供应商三维测试","highestEducation":"21","employeeID":"00808","orgNameZG":"华森集团MDM集成测试","salaryCardNumber":"6230520470062156875","recruitmentChannels":"04","isOnTheJob":"Y","salaryCardSubBranch":"中国农业银行双碑支行","salaryCardBank":"中国农业银行","highestVQ":"","positionCodeZG":"G00832","emergencyContactNum":"18716425723","depCodeZG":"23070601","registerResidenceNum":"90000004","registerResidence":"渝北","highestDegree":"","namePinyin":"huangwei","emergencyContact":"唐迎迎","sex":"M","orgCodeZG":"230706","highestPT":"","CODE":"huangwei","registerAddress":"90000004","politicalLandscape":"04","phoneNumber":"18225322775","nationality":"CHN","currentResidence":"重庆市江北区石马河街道国惠路32号8-9-2","name":"黄伟","nativePlace":"重庆合川","IDAddress":"重庆市江北区石马河街道国惠路32号8-9-2","employeeTypeZG":"正式员工","isSpecialGroup":"01","maritalStatus":"已婚","seniorityDate":"2023-04-24","positionNameZG":"测试2"}
         */

        private String pkId;
        @SerializedName("RY_ZT")
        private RyZtBean ry_zt;
        @SerializedName("RY_WORKINFO")
        private List<RyWorkinfoBean> ry_workinfo;

        @Data
        public static class RyZtBean implements Serializable {
            /**
             * natrueOfRegister : 0
             * documentType : 1
             * nation : 01
             * workEmail : huangwei@pharscin.com
             * documentNumber : 500382198704103418
             * birthdates : 1987-04-10
             * depNameZG : 供应商三维测试
             * highestEducation : 21
             * employeeID : 00808
             * orgNameZG : 华森集团MDM集成测试
             * salaryCardNumber : 6230520470062156875
             * recruitmentChannels : 04
             * isOnTheJob : Y
             * salaryCardSubBranch : 中国农业银行双碑支行
             * salaryCardBank : 中国农业银行
             * highestVQ :
             * positionCodeZG : G00832
             * emergencyContactNum : 18716425723
             * depCodeZG : 23070601
             * registerResidenceNum : 90000004
             * registerResidence : 渝北
             * highestDegree :
             * namePinyin : huangwei
             * emergencyContact : 唐迎迎
             * sex : M
             * orgCodeZG : 230706
             * highestPT :
             * CODE : huangwei
             * registerAddress : 90000004
             * politicalLandscape : 04
             * phoneNumber : 18225322775
             * nationality : CHN
             * currentResidence : 重庆市江北区石马河街道国惠路32号8-9-2
             * name : 黄伟
             * nativePlace : 重庆合川
             * IDAddress : 重庆市江北区石马河街道国惠路32号8-9-2
             * employeeTypeZG : 正式员工
             * isSpecialGroup : 01
             * maritalStatus : 已婚
             * seniorityDate : 2023-04-24
             * positionNameZG : 测试2
             */

            private String natrueOfRegister;
            private String documentType;
            private String nation;
            private String workEmail;
            private String documentNumber;
            private String birthdates;
            private String depNameZG;
            private String highestEducation;
            private String employeeID;
            private String orgNameZG;
            private String salaryCardNumber;
            private String recruitmentChannels;
            private String isOnTheJob;
            private String salaryCardSubBranch;
            private String salaryCardBank;
            private String highestVQ;
            private String positionCodeZG;
            private String emergencyContactNum;
            private String depCodeZG;
            private String registerResidenceNum;
            private String registerResidence;
            private String highestDegree;
            private String namePinyin;
            private String emergencyContact;
            private String sex;
            private String orgCodeZG;
            private String highestPT;
            @SerializedName("CODE")
            private String code;
            private String registerAddress;
            private String politicalLandscape;
            private String phoneNumber;
            private String nationality;
            private String currentResidence;
            private String name;
            private String nativePlace;
            private String IDAddress;
            private String employeeTypeZG;
            private String isSpecialGroup;
            private String maritalStatus;
            private String seniorityDate;
            private String positionNameZG;
        }

        @Data
        public static class RyWorkinfoBean implements Serializable {
            /**
             * orgName : 华森集团MDM集成测试
             * endDate :
             * abnormalEvents : 入职
             * changeType :
             * isInPosition : Y
             * positionCode : G00837
             * isTrial : N
             * depName : 测试02
             * depCode : 1009001
             * positionName : 软件测试工程师
             * isMainPosition : N
             * employeeType : 01
             * orgCode : 230706
             * startingDate : 2023-04-24
             */

            private String orgName;
            private String endDate;
            private String abnormalEvents;
            private String changeType;
            private String isInPosition;
            private String positionCode;
            private String isTrial;
            private String depName;
            private String depCode;
            private String positionName;
            private String isMainPosition;
            private String employeeType;
            private String orgCode;
            private String startingDate;
        }
    }
}
