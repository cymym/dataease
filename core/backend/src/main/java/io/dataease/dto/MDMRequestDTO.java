package io.dataease.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MDMRequestDTO implements Serializable {

    /**
     * batchId : uuid
     * items : {"RY_ZT":[{"name":"CODE","match":"SEQ","value":"huangwei"}]}
     * splitPage : {"pageNumber":"1","pageSize":"1"}
     */

    private String batchId;
    private ItemsBean items;
    private SplitPageBean splitPage;

    @Data
    public static class ItemsBean implements Serializable {
        @SerializedName("RY_ZT")
        private List<RyZtBean> ry_zt;

        @Data
        public static class RyZtBean implements Serializable {
            /**
             * name : CODE
             * match : SEQ
             * value : huangwei
             */

            private String name;
            private String match;
            private String value;
        }
    }

    @Data
    public static class SplitPageBean implements Serializable {
        /**
         * pageNumber : 1
         * pageSize : 1
         */

        private String pageNumber;
        private String pageSize;
    }
}
