export const columnOptions = [
  {
    label: 'commons.name',
    props: 'name'
  },
  {
    label: 'commons.description',
    props: 'description'
  },
  {
    label: 'commons.create_time',
    props: 'createTime'
  }
]

